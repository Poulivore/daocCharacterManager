using System;
using System.ComponentModel;
using System.Windows;   
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace daocCharacterManager {
    public partial class CharacterWindow :Grid 
    {

	private Character windowCharacter;

        public CharacterWindow()
        {
            InitializeComponent();
        }

	public void SetCharacter( Character character ) {
		windowCharacter = character;
		this.DataContext = character;
	}
    }
}
