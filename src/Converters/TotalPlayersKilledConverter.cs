using System;
using System.Windows.Data;

namespace daocCharacterManager
{
    public partial class TotalPlayersKilledConverter : IValueConverter {
                public object Convert(object value, Type targetType, object parameter,
                      System.Globalization.CultureInfo culture)        
        {            
            int totalPlayersKilled = ( int )value;

	    return TitleManager.GetTotalPlayersKilledPerc( totalPlayersKilled );
        }
        
        public object ConvertBack(object value, Type targetType, object parameter,
                    System.Globalization.CultureInfo culture) {
            return null; 
        }  
    }
}
