using System.Windows.Data;
using System.Windows.Media;

namespace daocCharacterManager {
    public  class TitleManager {
        public static decimal GetTotalPlayersKilledPerc( int kills ) {
			decimal percentage = 100;

			if( kills < 2000 ) {
				percentage = ((decimal)100 / 2000 ) * kills;
			} else if( kills < 25000 ) {
				percentage = ((decimal) 100 / 25000 ) * kills;
			} else if( kills < 100000 ) {
				percentage = ((decimal) 100 / 100000 ) * kills;
			} else if( kills < 500000 ) {
				percentage = ((decimal) 100 / 500000 ) * kills;
			}

			return percentage;
		}
	
        public static decimal GetTotalDeathblowsPerc( int kills ) {
			decimal percentage = 100;

			if( kills < 2000 ) {
				percentage = ((decimal)100 / 2000 ) * kills;
			} else if( kills < 25000 ) {
				percentage = ((decimal) 100 / 25000 ) * kills;
			} else if( kills < 100000 ) {
				percentage = ((decimal) 100 / 100000 ) * kills;
			}

			return percentage;
		}
	
        public static decimal GetTotalSoloKillsPerc( int kills ) {
			decimal percentage = 100;

			if( kills < 2000 ) {
				percentage = ((decimal)100 / 2000 ) * kills;
			} else if( kills < 25000 ) {
				percentage = ((decimal) 100 / 25000 ) * kills;
			}

			return percentage;
		}

		public static object GetTitleMasterSoldierColor( int kills ) {
			if( kills >= 2000 ) {
			return new SolidColorBrush(Colors.Green);
			}

			return Binding.DoNothing;
		}

		public static object GetTitleMasterEnforcerColor( int kills ) {
			if( kills >= 25000 ) {
			return new SolidColorBrush(Colors.Green);
			}

			return Binding.DoNothing;
		}

		public static object GetTitleMasterAssassinColor( int kills ) {
			if( kills >= 100000 ) {
			return new SolidColorBrush(Colors.Green);
			}

			return Binding.DoNothing;
		}

		public static object GetTitleMasterExecutionerColor( int kills ) {
			if( kills >= 500000 ) {
			return new SolidColorBrush(Colors.Green);
			}

			return Binding.DoNothing;
		}

		public static object GetTitleLoneEnforcerColor( int kills ) {
			if( kills >= 2000 ) {
				return new SolidColorBrush(Colors.Green);
			}

			return Binding.DoNothing;
		}
    }
}
